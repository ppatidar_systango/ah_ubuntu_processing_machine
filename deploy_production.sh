#!/bin/sh
cd /home/ubuntu/processing_machine/ah_ubuntu_processing_machine
. ah_venv_production/bin/activate
# export DJANGO_SETTINGS_MODULE=active_hospitality.settings.production
#git pull origin master
pip install -r requirements.txt
# python manage.py migrate
# python manage.py syncdb
process_id=$(netstat -tupln | grep 9006 | grep -o "[0-9]\+/" | grep -o "[0-9]\+")
kill -9 $process_id
echo "Process id: $process_id"
process_id1=$(netstat -tupln | grep 9006 | grep -o "[0-9]\+/" | grep -o "[0-9]\+")
kill -9 $process_id1
echo "Process id: $process_id1"
process_id2=$(netstat -tupln | grep 9006 | grep -o "[0-9]\+/" | grep -o "[0-9]\+")
echo "Process id: $process_id2"

{ gunicorn -b 127.0.0.1:9006 --workers=1 active_hospitality.wsgi:application ;} &
echo "Restarted server successfully"
#kcron active
python manage.py installtasks