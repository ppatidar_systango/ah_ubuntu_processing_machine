from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ah_ubuntu.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^media_conversion/',include('media_conversion.urls',namespace="media_conversion")),
    url(r'^admin/', include(admin.site.urls)),
)
