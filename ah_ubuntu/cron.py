import kronos
import logging
import memcache
import urllib2
import urllib
from django.conf import settings
from ah_ubuntu import util


logger = logging.getLogger("cron_log")


import os
import boto
import datetime
import subprocess
import re
import json
from copy import deepcopy
from PIL import Image

@kronos.register('* * * * *')
def set_to_conversion():
  try:
    logger.info("hello Successfully...")
    mem_s = memcache.Client(["127.0.0.1:11211"])  
    memcache_data = mem_s.get("memcache_data_to_convert_content")
    logger.info(" memcache data to convert content : %s"%(memcache_data)) 
    mem_s.delete("memcache_data_to_convert_content")
    if memcache_data:
      for key, value in memcache_data.items():
        try:
          post_type_id = key
          s3_response = value
          s3_bucket = value['s3_bucket']
          s3_url = value['s3_url']
          s3_key = value['s3_key']
          post_id = value['post_id']
          post_type = value['post_type']
          post_media_type = value['post_media_type']
          file_name = value['file_name']
          s3_frame_bucket = value['s3_frame_bucket']
          env_response_url = value['env_response_url']
          restaurant_id = value['restaurant_id']
          user_list = value['user_list']

          if post_media_type =="video":
            response_data = {}
            response_data['image_s3_key'] = get_s3_video_frame(s3_key,s3_url,file_name,s3_frame_bucket)
            response_data['convert_video_s3_key'] = convert_mp3_to_codec(s3_key,s3_bucket)
            response_data['status'] = True

            response_data['post_id'] = post_id
            response_data['post_type'] = post_type
            response_data['post_media_type'] = post_media_type
            response_data['restaurant_id'] = restaurant_id
            response_data['user_list'] = user_list

          if post_media_type == "image":
            response_data = deepcopy(resize_image(s3_key,s3_bucket))
            response_data['status'] = True

            response_data['post_id'] = post_id
            response_data['post_type'] = post_type
            response_data['post_media_type'] = post_media_type
            response_data['restaurant_id'] = restaurant_id
            response_data['user_list'] = user_list


        except Exception, e:
          logger.info(" Exception11 : %s"%(e))
          response_data['status'] = False
          
        if response_data:
          url = env_response_url
          logger.info(" url : %s"%(url))
          request = urllib2.Request(url, data=urllib.urlencode(response_data))
          response = urllib2.urlopen(request).read()
          logger.info(" response : %s"%(response))
  except Exception, e:
    logger.info(" Exception : %s"%(e)) 



def get_s3_video_frame(s3_key,s3_url,file_name,s3_frame_bucket):
    logger.info(" get_s3_video_frame ") 
    current_timestamp = util.get_millisecond_for_utc_date(datetime.datetime.now())
    s3_video_path = s3_url+s3_key
    frame_time = "00:00:05"
    s3_filename = file_name
    s3_frame_key = "Video_frame_"+s3_filename+str(current_timestamp)+".jpeg"
    temp_frame_image_path = settings.MEDIA_ROOT+"/temp_cover_frame.jpeg"
    ffmpeg_list = ["ffmpeg", "-i", s3_video_path, "-ss", frame_time, "-vframes", "1", "-f", "image2", temp_frame_image_path]
    subprocess.call(ffmpeg_list)
    logger.info(" settings.AWS_ACCESS_KEY_ID : %s"%(settings.AWS_ACCESS_KEY_ID))
    logger.info(" settings.AWS_SECRET_ACCESS_KEY : %s"%(settings.AWS_SECRET_ACCESS_KEY))
    logger.info(" s3_frame_bucket : %s"%(s3_frame_bucket))
    logger.info(" s3_frame_key : %s"%(s3_frame_key))
    s3 = boto.connect_s3(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY)
    bucket = s3.get_bucket(s3_frame_bucket)
    key = bucket.new_key(s3_frame_key)
    key.set_contents_from_filename(temp_frame_image_path)
    key.set_acl('public-read')
    os.remove(temp_frame_image_path)

    return s3_frame_key

def download_content_file(s3_key, s3_bucket, temp_file_path):
  conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY)
  bucket = conn.get_bucket(s3_bucket)
  boto_key = bucket.get_key(s3_key)
  boto_key.get_contents_to_filename(temp_file_path)
  return temp_file_path


def get_file_info(file_path):
  file_detail = subprocess.check_output("ffprobe -v quiet -print_format json -show_streams " + file_path, shell=True)
  file_detail = json.loads(file_detail)
  print file_detail
  file_info = {
    "video" : [],
    "audio" : []
  }
  for index, stream in enumerate(file_detail["streams"]):
    field_name = 'audio' if stream["codec_type"] == "audio" else 'video'
    info_dict = {
      "codec" : stream["codec_name"] if stream.has_key("codec_name") else None,
      "codec_type" : stream["codec_type"]
    }
    if stream["codec_type"] == "video" and stream.has_key("profile"):
      info_dict["profile"] = stream["profile"].lower()
    file_info[field_name].append(info_dict)
    if index == 0:
      file_info['duration'] = stream["duration"]
  return file_info


def convert_final_video(s3_Key_value, temp_file_name, bucket, audio_codec):
  temp_converted_file_name = "temp_conveted_m4a_" + s3_Key_value
  temp_converted_file_path = settings.MEDIA_ROOT + "/temp_conveted_m4a_" + s3_Key_value
  print "temp_converted_file_path", temp_converted_file_path

  subprocess.call([ "ffmpeg", "-i", 
                    temp_file_name, 
                    "-acodec", audio_codec, 
                    "-vcodec", "libx264",
                    "-vprofile","Baseline",
                    "-strict","-2", 
                    temp_converted_file_path
                 ])

  key = bucket.new_key(temp_converted_file_name)
  key.set_contents_from_filename(temp_converted_file_path)
  key.set_acl('public-read')
  os.remove(temp_file_name)
  os.remove(temp_converted_file_path)
  return temp_converted_file_name


def music_video_convertion(info, s3_Key_value, temp_file_name, bucket):
  video_detail, audio_detail = info["video"], info["audio"]
  print "audio codecs : %s and video codecs : %s"%(audio_detail, video_detail)
  audio_codec = audio_detail[0]["codec"] if len(audio_detail) > 0 else None
  if len(video_detail) > 0:
    video_codec = video_detail[0]["codec"]
    profile_name = video_detail[0]["profile"] if video_detail[0].has_key("profile") else ''
  else :
    video_codec, profile_name = None, ''

  print "audio codec : %s and video codec : %s and profile : %s"%(str(audio_codec), str(video_codec), str(profile_name))
  converted_s3_key = None
  print "converting music video"
  print "s3_Key_value is : %s"%(s3_Key_value)
  print "temp_file_name : %s"%(temp_file_name)
  if video_codec and audio_codec:
    print "video codec : %s, audio codec : %s"%(video_codec, audio_codec)
    
    if video_codec == "h264" and audio_codec == "aac":
      if not "baseline" in profile_name:
        print "Converting the profile to Baseline" 
        aac_codec = 'aac' if audio_codec != "aac" else "copy"
        converted_s3_key = convert_final_video(s3_Key_value, temp_file_name, bucket, aac_codec)
      else:
        print "No need to convert music video and audio codec"
        os.remove(temp_file_name)                
    else:
      print "Convert video for both audio and video codec"
      audio_codec = 'aac' if audio_codec != "aac" else "copy"
      converted_s3_key = convert_final_video(s3_Key_value, temp_file_name, bucket, audio_codec)
  elif video_codec and not audio_codec:
    print "video codec : %s"%(video_codec)
    
    if video_codec == "h264":
      if not "baseline" in profile_name:
        print "Converting the profile to Baseline" 
        converted_s3_key = convert_final_video(s3_Key_value, temp_file_name, bucket, audio_codec="copy")   
      else:    
        print "No need to convert music video"
        os.remove(temp_file_name)        
    else:
      print "Video codec is not h264"
      temp_converted_file_name = "temp_conveted_m4a_" + s3_Key_value
      temp_converted_file_path = settings.MEDIA_ROOT + "/temp_conveted_m4a_" + s3_Key_value
      print "temp_converted_file_path", temp_converted_file_path
      converted_s3_key = convert_final_video(s3_Key_value, temp_file_name, bucket, audio_codec="aac")
  else :
    print "No video audio codecs attached"
    os.remove(temp_file_name)

  return converted_s3_key


def convert_mp3_to_codec(s3_key,s3_video_bucket):
    logger.info(" get_s3_video_frame ") 
    conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY)
    bucket = conn.get_bucket(s3_video_bucket)  

    filtered_s3_Key = re.sub('[^A-Za-z0-9_+()-.]+', '', s3_key)
    temp_name = settings.MEDIA_ROOT+"/temp_" + re.sub('[^A-Za-z0-9_+()-.]+', '', filtered_s3_Key)
    temp_file_name = download_content_file(s3_key, s3_video_bucket, temp_name)
    file_info = get_file_info(temp_file_name)

    converted_s3_key = music_video_convertion(file_info, filtered_s3_Key, temp_file_name, bucket=bucket)

    if converted_s3_key :
        return converted_s3_key
    else:
        return s3_key


def resize_image(s3_key,s3_image_bucket):
    response_data = {}
    current_timestamp = util.get_millisecond_for_utc_date(datetime.datetime.now())
    image_s3_key = s3_key
    temp_download_file = settings.MEDIA_ROOT + "/download_image.jpeg"
    temp_image = download_content_file(image_s3_key,s3_image_bucket,temp_download_file) 
    image = Image.open(temp_image)
    width, height = image.size
    new_width =float(720)
    width = float(width)
    height = float(height)
    if new_width < width:
      new_height = new_width * (height/width)
    else :
      new_height = height
      new_width = width

    s3 = boto.connect_s3(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY)
    bucket = s3.get_bucket(s3_image_bucket)

    resize_image_mobile_file = 'resize_image_mobile'+str(current_timestamp)+'.jpg'
    resize_image_mobile = settings.MEDIA_ROOT + "/" +resize_image_mobile_file
    image = image.resize((int(new_width), int(new_height)), Image.ANTIALIAS)
    image = image.convert('RGB')
    image.save(resize_image_mobile, "JPEG")

    key = bucket.new_key(resize_image_mobile_file)
    key.set_contents_from_filename(resize_image_mobile)
    key.set_acl('public-read')
    os.remove(resize_image_mobile)

    resize_image_web_file = 'resize_image_web'+str(current_timestamp)+'.jpg'
    resize_image_web = settings.MEDIA_ROOT + "/" +resize_image_web_file
    image = image.resize((int(new_width), int(new_height)), Image.ANTIALIAS)
    image = image.convert('RGB')
    image.save(resize_image_web, "JPEG")

    key = bucket.new_key(resize_image_web_file)
    key.set_contents_from_filename(resize_image_web)
    key.set_acl('public-read')
    os.remove(resize_image_web)

    original_key = s3.get_bucket(s3_image_bucket).get_key(s3_key)
    original_key.delete()

    response_data['convert_image_s3_key'] = resize_image_mobile_file
    response_data['image_s3_key'] = resize_image_web_file
    response_data['convert_image_s3_width'] = int(new_width)
    response_data['convert_image_s3_height'] = int(new_height)

    return response_data
