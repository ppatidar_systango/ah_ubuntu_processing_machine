import datetime





def get_millisecond_for_utc_date(input_date):
    start_date = datetime.datetime(1970,1,1)
    input_date = input_date.replace(tzinfo=None)
    dt = input_date - start_date
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + ( dt.microseconds / 1000.0 )
    return int(ms)