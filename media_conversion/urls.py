from django.conf.urls import patterns, url
from media_conversion.views import *


urlpatterns = patterns('',
	url(r'^convert_media/$', ConvertMedia.as_view(), name='ConvertMedia'),
)