from django.shortcuts import render

# Create your views here.
from django.views.generic import View
from django.shortcuts import HttpResponse
import json
import memcache

from django.views.decorators.csrf import csrf_exempt
from inspect import isfunction
from django.utils.decorators import method_decorator




class _cbv_decorate(object):
    def __init__(self, dec):
        self.dec = method_decorator(dec)

    def __call__(self, obj):
        obj.dispatch = self.dec(obj.dispatch)
        return obj


def patch_view_decorator(dec):
    def _conditional(view):
        if isfunction(view):
            return dec(view)
        return _cbv_decorate(dec)(view)
    return _conditional


    
@patch_view_decorator(csrf_exempt)
class ConvertMedia(View):
    def post(self, request):
        print 'ConvertMedia'
        post_id = request.POST.get("post_id")
        post_type_id = request.POST.get("post_type_id")
        post_type = request.POST.get("post_type")
        s3_key = request.POST.get("s3_key")
        s3_url = request.POST.get("s3_url")
        s3_bucket = request.POST.get("s3_bucket")
        post_media_type = request.POST.get("post_media_type")
        file_name = request.POST.get("file_name")
        s3_frame_bucket = request.POST.get("s3_frame_bucket")
        restaurant_id = request.POST.get("restaurant_id")
        user_list = request.POST.get("user_list")
        env_response_url = request.POST.get("env_response_url")

        

        if post_id and s3_key and s3_bucket and post_type_id and post_type:
            mem_s = memcache.Client(["127.0.0.1:11211"])
            memcache_data = mem_s.get("memcache_data_to_convert_content")
            print memcache_data
            content_data = { 
                "s3_key" : s3_key,
                "s3_url" : s3_url,
                "s3_bucket" : s3_bucket,
                "post_id":post_id,
                "post_type":post_type,
                "post_media_type" : post_media_type,
                "file_name" : file_name,
                "s3_frame_bucket" : s3_frame_bucket,
                "restaurant_id" : restaurant_id,
                "user_list" : user_list,
                "env_response_url" : env_response_url
            }

            if not memcache_data:
                memcache_data = {post_type_id : content_data}
            else:
                memcache_data[post_type_id] = content_data
            mem_s.set("memcache_data_to_convert_content", memcache_data)
            
            response_data = {"status": True,
                             "msg" :"add content for converting successfully"
                            }
        else:
            response_data = {"status": False, 
                               "error" :"Invalid content detail found"
                              }
        print response_data
        return HttpResponse(json.dumps(response_data), content_type="application/json")